# Instructions for build/run

## Prerequisites

Docker must be set up, i.e. able to build and execute images

You must be able to execute x86-64 images

## Build

In the project root folder, run

    docker build -t jg-dotnetapp .

## Configure LaunchDarkly

In the LaunchDarkly console, create a new project (or select an existing project) and create at least one environment (e.g. Production).

In the project root folder, replace the LaunchDarkly SDK key below with the appropriate environment SDK key (e.g. Production) and run

    cat << EOF > appsettings.json
    {
        "Logging": {
            "LogLevel": {
                "Default": "Information",
                "Microsoft.AspNetCore": "Warning"
            }
        },
        "AllowedHosts": "*",
        "LaunchDarklyKey": "<copy LaunchDarkly SDK key here>"
    }
    EOF

This should create a settings file that will contain the SDK key for your account.

In your LaunchDarkly console, create a feature flag in Production with Name and Key set to `TurnOffTitle`. The other settings can remain default (boolean, etc.).

## Execute the container

Run the docker container with the appsettings file mounted:

    docker run --rm -it -p 8000:80 --mount type=bind,source=$(pwd)/appsettings.json,target=/app/appsettings.json jg-dotnetapp:latest

Open your browser and browse to http://<hostname>:8000 (if you are running locally, <hostname> may be localhost, depending on your local network settings).

In your LaunchDarkly console, toggle the feature flag off and back on again, refreshing the open page in your browser between changes, to verify that the title is toggled off and on.